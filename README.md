# AWS

GitLab is an Advance Tier Partner with AWS and currently holds a DevOps Competency.  

This is the highest tier of partner that an AWS ISV Partner can hold.  The AWS Competency programs are some of the toughest to obtain in the industry. AWS makes the [requirments](https://s3-us-west-2.amazonaws.com/competency.awspartner.com/DevOps/AWS+DevOps+Competency+Consulting+Partner+Validation+Checklist.pdf) public. 

Below are links to help you understand the depth of relationship and gain knowledge about how GitLab and AWS work well together.

## Dedicated Pages

- [AWS Landing Page](https://about.gitlab.com/solutions/aws/)
- [GitLab AWS Marketplace Listing Page](https://aws.amazon.com/marketplace/seller-profile?id=9657c703-ca56-4b54-b029-9ded0fadd970&ref=dtl_B071RFCJZK)
- [Purchasing GitLab via a Private Offer on AWS Marketplace](https://aws.amazon.com/blogs/awsmarketplace/how-subscribe-aws-marketplace-seller-private-offers/)
- [AWS Marketplace Information for Buyers of GitLab via AWS Marketplace](https://docs.aws.amazon.com/marketplace/latest/buyerguide/buyer-private-offers.html)
- [AWS GitLab Partner Solution Finder Page](https://aws.amazon.com/partners/find/partnerdetails/?n=GitLab%2C%20Inc.&id=001E0000018YWFfIAO) 
- [Application Deployment Targets - scroll down below the fold](https://about.gitlab.com/product/deploy-targets) 

## Blogs

- [Choosing a CI/CD approach: Open Source on AWS, an Iponweb story](https://aws.amazon.com/blogs/devops/choosing-a-ci-cd-approach-open-source-on-aws-an-iponweb-story/)
- [How to use GitLab's Incident Management with AWS CloudWatch](https://about.gitlab.com/blog/2020/10/08/incident-management-with-aws-cloudwatch/)
- [AWS Partner Network (APN) Blog: Using GitLab CI/CD Pipeline to Deploy AWS SAM Applications ](https://aws.amazon.com/blogs/apn/using-gitlab-ci-cd-pipeline-to-deploy-aws-sam-applications/)
- [How to create a Kubernetes cluster on Amazon EKS in GitLab](https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to)
- [From monolith to microservices: How to leverage AWS with GitLab](https://about.gitlab.com/blog/2020/03/24/from-monolith-to-microservices-how-to-leverage-aws-with-gitlab)
- [Simple deployments to Amazon EKS](https://about.gitlab.com/releases/2018/06/06/eks-gitlab-integration/)
- [GitLab adds support to auto create AWS EKS cluster](https://about.gitlab.com/releases/2019/11/22/gitlab-12-5-released/)
- [Launching on AWS Marketplace](https://about.gitlab.com/blog/2019/06/11/gitlab-on-aws-marketplace/)
- [GitLab’s AWS Lambda usage survey results](https://about.gitlab.com/blog/2019/11/27/aws-lambda-usage-stats/)
- [How to set up multi-account AWS SAM deployments with GitLab](https://about.gitlab.com/blog/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/)
- [Highlights from AWS re:Invent 2019](https://about.gitlab.com/blog/2019/12/13/updates-from-aws-reinvent/)
- [GitLab achieves AWS DevOps Competency certification](https://about.gitlab.com/blog/2018/11/28/gitlab-achieves-aws-devops-competency-certification/)


## Technical Enablement

### Youtube Playlists

- [GitLab and AWS](https://youtube.com/playlist?list=PL05JrBw4t0Ko30Bkf8bAvR-8E441Fy2G9)
- [GitLab Alliances Tech](https://youtube.com/playlist?list=PL05JrBw4t0KqaWCrU4avIY6TQveKTDMso)
- [GitLab Reference Architecture, Performance Testing, Scaling and HA](https://youtube.com/playlist?list=PL05JrBw4t0KqgVugqCld2fUvrBulanchq)
- [GitLab and Kubernetes](https://youtube.com/playlist?list=PL05JrBw4t0Kqp16k3nHTMdrWdATrcDZ5C)
- Learning Path (Ordered Youtube Playlist): [Learning to Provision the AWS Quick Start for GitLab on EKS](https://bit.ly/3iM2Bx7)
  
  The Playlist consists of:

    - Video 1: [Gitlab Reference Architectures, Cloud Native Hybrid and What is Gitaly (11mins)](https://bit.ly/37e3c44)
    - Video 2: [Overview GitLab AWS Implementation Patterns (13mins)](https://bit.ly/3lpGwpZ)
    - Video 3: [Overview GitLab AWS Quick Start for GitLab Cloud Native Hybrid on EKS (9mins)](https://bit.ly/3lrv6C0)
    - Video 4: [Provisioning Ready-to-Run GitLab for 50,000 Users in 14 Clicks and a Long Lunch (20mins)](https://bit.ly/3mbTqIr)
    - Video 5: [Performance Testing an AWS Quick Start Provisioned GitLab Cloud Native Hybrid Instance (32mins)](https://bit.ly/2VYtW6c)


### Using GitLab with AWS

- [How to create a Kubernetes cluster on Amazon EKS in GitLab](https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to)
- [Deploying AWS Lambda Functions using GitLab CI/CD](https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html#deploying-aws-lambda-function-using-GitLab-cicd)
- [Monitoring AWS Resources](https://docs.gitlab.com/ee/user/project/integrations/prometheus_library/cloudwatch.html#requirements)
- [Location-aware Git remote URL with AWS Route53](https://docs.gitlab.com/ee/administration/geo/replication/location_aware_git_url.html#location-aware-git-remote-url-with-aws-route53-premium-only)
- [Adding and Removing Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html)

### Provisioning GitLab on AWS

- [Installing GitLab HA on AWS](https://docs.gitlab.com/ee/install/aws/)
- **[GitLab HA Scaling Runner Vending Machine for AWS](https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg#easy-buttons-provided)**
- **[Provisioning 100 GitLab Spot Runners on AWS in Less Than 10 Mins via Less Than 10 Clicks For $5/hr](https://youtu.be/EW4RJv5zW4U)**
- [IAM roles for AWS](https://docs.gitlab.com/charts/advanced/external-object-storage/aws-iam-roles.html#iam-roles-for-aws)
- [Autoscaling GitLab Runners on AWS](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/index.html#autoscaling-gitlab-runner-on-aws)
- [GitLab Reference Architectures, Performance Testing, Cloud Native Hybrid and Gitaly](https://www.youtube.com/watch?v=1TYLv2xLkZY)
- [Overview GitLab AWS Implementation Patterns](https://youtu.be/_x3I1aq7fog)
- [Overview AWS Quick Start for Cloud Native Hybrid on EKS](https://youtu.be/XHg6m6fJjRY)


## Customer Case Studies

- [Goldman Sachs](https://about.gitlab.com/customers/goldman-sachs/)
- [Axway](https://about.gitlab.com/customers/axway/)
- [Ticketmaster](https://about.gitlab.com/blog/2017/06/07/continous-integration-ticketmaster/)
- [BI Worldwide](https://about.gitlab.com/customers/bi_worldwide/)
- [Trek10](https://about.gitlab.com/customers/trek10/)

## External Content and Blogs

- [GitLab's GitOps Presentation from re:Invent 2020](https://www.youtube.com/watch?v=QgD8Jz22TXg) Presented by William Chia and Brendan O'Leary from GitLab (Nov. 2020)
- [[WEBCAST] Deploy AWS Lambda applications with ease - Webinar hosted by AWS and GitLab](https://learn.gitlab.com/c/deploy-aws-lambda-ap?x=04KSqy)  Presented by AWS and GitLab
- [[WEBCAST] How Trek10 modernizes Application Development using GitLab CI/CD on AWS](https://learn.gitlab.com/c/dvo7vj6ziky?x=04KSqy)   Presented by AWS, GitLab and Trek 10
- [AWS Marketplace Series: How to simplify your software delivery toolchain on AWS](https://pages.awscloud.com/awsmp-ss-dev-GitLab-SoftwareDeliveryToolchain.html) AWS Lead Webinar on CI/CD highlighting GitLab.  Marc Hornbeek, Ambassador, DevOps Institute and Kenneth Walsh, Senior Solutions Architect, AWS (June 2020)
- [Serverless GitLab CI/CD on AWS Fargate](https://medium.com/ci-t/serverless-gitlab-ci-cd-on-aws-fargate-da2a106ad39c), Daniel Coutinho de Miranda, CI&T (June 24,2020)
- [Reference Architecture for GitLab Runners in AWS](https://medium.com/better-programming/reference-architecture-for-gitlab-runners-in-aws-829c45f857ed), Akshay Kapoor
 (April 21, 2020)
- [Wag! Reduced their Release Process to Just 1 Minute with GitLab CI/CD](https://pages.awscloud.com/apn-tv-voice-of-the-customer-ep-002.html?trk=tv_card&did=tv_card), AWS APN TV (April 5, 2020)
- [Deploying and Managing AWS Lambda With GitLab CI and Terraform](https://www.hashicorp.com/resources/deploying-and-managing-aws-lambda-with-gitlab-ci-and-terraform), HashiCorp (Feb 27, 2020)
- [CI/CD using GitLab, AWS, EKS, Kubernetes and ECR](https://medium.com/faun/ci-cd-using-gitlab-aws-eks-kubernetes-and-ecr-a55ff757b921), Suleman Hasib (Jan 7 2020)
- [How to Build Your First GitLab CI/CD Pipeline ](https://medium.com/better-programming/how-to-build-your-first-gitlab-ci-cd-pipeline-5416d7e3b602), Andrew Bestbier (Nov 5, 2019)
- [Configuring .gitlab-c1.yml with AWS EC2 for Continuous Integration (CI) or Continuous Deplyment (CD)](https://medium.com/hackernoon/configuring-gitlab-ci-yml-150a98e9765d), Jose Javi Asilis (Apr 22, 2018)
- [Configuring GitLab CI on AWS EC2 Using Docker](https://medium.com/hackernoon/configuring-gitlab-ci-on-aws-ec2-using-docker-7c359d513a46), Jose Javi Asilis
 (April 22, 2018)
- [How to deploy Node.js app on AWS with GitLab](https://medium.com/@adhasmana/how-to-deploy-node-js-app-on-aws-with-gitlab-24fabde1088d), Abhinav Dhasmana (May 3, 2018)
- [Building a continuous delivery pipeline with GitLab](https://medium.com/@obezuk/building-a-continuous-delivery-pipeline-with-gitlab-ci-cloudflare-and-aws-955846944e7e), Tim Obezuk (Mar 5, 2018)
- [Deployment Automation with GitLab Runner + AWS](https://medium.com/hackernoon/configuring-gitlab-ci-yml-150a98e9765d), Alfiana Sibuea (Nov 22, 2018)
- [A Comprehensive Guide to Running GitLab on AWS](https://medium.com/@aloisbarreras_18569/a-comprehensive-guide-to-running-gitlab-on-aws-bd05cb80e363), Alois Barreras (Apr 17, 2018)
- [DevOps by GitLab & Kubernetes on AWS](https://medium.com/@didanaiavs/devops-by-gitlab-kubernetes-on-aws-2ef9f7c1e3eb), Didanai J. (Sep 7, 2018)
- [How To Deploy from GITLAB TO AWS FARGATE](https://webcaptioner.com/blog/2017/12/deploy-from-gitlab-to-aws-fargate/), Web Captioner (Dec 28, 2017)
- [Using Gitlab CI with AWS Container Registry](https://medium.com/@stijnbe/using-gitlab-ci-with-aws-container-registry-ecaf4a37d791), Stijn Beauprez (Oct 20, 2017)
